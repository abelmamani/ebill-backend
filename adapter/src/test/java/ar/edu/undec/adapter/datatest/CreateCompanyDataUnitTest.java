package ar.edu.undec.adapter.datatest;

import ar.edu.undec.adapter.data.company.crud.CreateCompanyCRUD;
import ar.edu.undec.adapter.data.company.model.CompanyEntity;
import ar.edu.undec.adapter.data.company.repoimplementation.CreateCompanyRepoImplementation;
import edu.undec.ebill.company.model.Company;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CreateCompanyDataUnitTest {

    @InjectMocks
    CreateCompanyRepoImplementation createCompanyRepoImplementation;
    @Mock
    CreateCompanyCRUD createCompanyCRUD;

    @Test
    public void saveCompany_CompanySaved_ReturnID() {
        when(createCompanyCRUD.save(any(CompanyEntity.class))).thenReturn(new CompanyEntity(1, "A", "BBB222"));
        Integer result = createCompanyRepoImplementation.saveCompany(Company.instanceCore(null, "A", "BBB222"));
        Assertions.assertEquals(1, result);
    }

    @Test
    public void saveCompany_DBException_Return0() {
        doThrow(RuntimeException.class).when(createCompanyCRUD).save(any(CompanyEntity.class));
        Integer result = createCompanyRepoImplementation.saveCompany(Company.instanceCore(null, "A", "BBB222"));
        Assertions.assertEquals(0, result);
    }
}
