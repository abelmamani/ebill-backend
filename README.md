# ebill-backend

## Descripcion

Proyecto proveedor de facturas electronicas.

## Stack utilizado

Maven, Java, Spring Boot.

## Arquitectura

Clean Architecture - Principios SOLID - Patrones de Diseño

## Buenas prácticas y conceptos a considerar

- La nomenclatura de paquetes será en minúsculas
- La nomenclatura de clases será en UpperCamelCase
- La nomenclatura de métodos será en lowerCamelCase
- La organización de paquetes será por modelo->aspecto, tanto a nivel src/main como a nivel src/test. Ejemplo:
  ```
  computadora
  └─ excepciones
  └─ modelo
  └─ repositorio
  └─ casodeuso
  ```
- Se deben crear pruebas unitarias tanto a nivel Core como a nivel Adapter
- Usar Excepciones personalizadas
- Se debe usar método factory/instancia para crear objetos
- Nomenclatura representativa de clases, métodos, etc.

## DER Conceptual

![alt text](<./der-ebill.png>)

## Restricciones

- No puede existir más de una Company con el mismo name.
- No puede existir más de una Company con el mismo reference
- No puede existir más de una Bill de la misma Company con el mismo number.
- El atributo expiration no puede ser menor a la fecha actual (al crear una Bill).
- Atributos obligatorios de Company: name, reference.
- Atributos obligatorios de Bill: number, amount, expiration, status, client, company.

## Casos de uso

- Registrar una Company.
- Registrar una Bill.
- Consultar la lista de Bill de un client (mail, companyName), este tambien puede recibir filtros por status (paid, unpaid).
- Consultar Bill de un cliente por vencer, devuelve siempre un solo registro (mail, companyName)
- Registrar el pago de una Bill (mail, numberBill), esto hace el cambio de estado de la Bill.

## Funcionalidades

- Company (Crear)
    - Endpoint [POST]: http://localhost:8080/company
    - RequestBody:
  ```json
    {
      "name": "IpTel",
      "reference": "AAA-100"
    }
    ```
    - Response:
  ```json
    1
  ```

- Company (Consultar)
    - Endpoint [GET]: http://localhost:8080/company
    - Response:
  ```json
  [  
    {
      "id": 200,    
      "name": "IpTel",
      "reference": "AAA-100"
    },
    {
      "id": 300,    
      "name": "Clao",
      "reference": "ABC-560"
    }
  ]
  ```

- Bill (Consultar)
    - Endpoint [GET]: http://localhost:8080/bill
    - RequestBody:
  ```json
    {
      "email": "name@mail.com"
    }
    ```
    - Response:
  ```json
    [
      {
        "number": 1000001,
        "description": "MAY/2023",
        "amount": 12500,
        "expiration": "10/06/2023",
        "status": "unpaid",
        "client": "name@mail.com",
        "companyName": "IpTel"
      },
      {
        "number": 1000002,
        "description": "MAY/2023",
        "amount": 6500,
        "expiration": "08/06/2023",
        "status": "unpaid",
        "client": "name@mail.com",
        "companyName": "Claro"
      }
    ]
    ```

- Bill (Pagar)
    - Endpoint [PUT]: http://localhost:8080/bill
    - RequestBody:
  ```json
    {
      "email": "name@mail.com"
    }
    ```
    - Response:
  ```json
    true
  ```