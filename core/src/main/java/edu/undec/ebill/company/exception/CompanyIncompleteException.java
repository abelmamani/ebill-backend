package edu.undec.ebill.company.exception;

public class CompanyIncompleteException extends RuntimeException {
    public CompanyIncompleteException(String message) {
        super(message);
    }
}
